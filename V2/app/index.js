'use strict';
if(window.location.hostname == 'localhost'){
  var apiUrl        = 'http://localhost/simb/api/V1'
  var socket_server = 'http://localhost:3001'
}else{
  var apiUrl        = 'http://api.simb.inf.br'
  var socket_server = 'http://node.simb.inf.br'
}
// var socket = io(socket_server, {transports: ['websocket', 'polling', 'flashsocket']});
var socket = io.connect( socket_server )


const app = angular.module('app', [
  'ui.router',
  'ngAnimate',
  'toastr',
  'oitozero.ngSweetAlert',
  'ui.bootstrap',
  'ui.calendar',
  'angularFileUpload',
  'ngSanitize',
  
])
.run([
  '$rootScope',
  appRun,
  

])

function appRun($rootScope) {
  $rootScope.loading        = false;
  $rootScope.callSetLoading = 0;
  $rootScope.me = {}
}
