(function(){
  app.controller('funilCtrl', [
    '$http',
    'msgs',
    '$filter',
    '$location',
    'session',
    'api',
    'token',
    funilController
  ])

  function funilController($http, msgs, $filter, $location, session, api, token){
    if(!session.isLogged()) { return false; }

    const url        = `${apiUrl}/funil`
    var vm           = this
    vm.me            = token.get_token().data
    vm.pageSize      = 10;
    vm.currentPage_1 = 1;
    vm.currentPage_2 = 1;
    vm.currentPage_3 = 1;
    vm.currentPage_4 = 1;
    vm.search_match  ={};
    vm.get_me = () => {
      api.get(`${apiUrl}/me`)
        .then(function(response){
          vm.me = response.data
        })
    }
    vm.refresh = function(){
      vm.get_me();
      api.get(url)
        .then(function success(response){
          vm.fechado     = $filter('number')(response.data.negociations.fechado, 2);
          vm.ganhos      = $filter('number')(response.data.negociations.ganhos, 0);
          vm.perdidos    = $filter('number')(response.data.negociations.perdidos, 0);
          vm.conversao   = $filter('number')(response.data.negociations.conversao, 2);

          vm.negociations_1 = $filter('filter')(response.data.results, {'step':'0'});
          vm.negociations_2 = $filter('filter')(response.data.results, {'step':'1'});
          vm.negociations_3 = $filter('filter')(response.data.results, {'step':'2'});
          vm.negociations_4 = $filter('filter')(response.data.results, {'step':'3'});
        })
    }
    vm.change_temperature = function(negociation){
      negociation.change_temperature = !negociation.change_temperature
    }
    vm.set_temperature = function(negociation, temperature){
      negociation.negociation_temperature = temperature;
      negociation.change_temperature = false
      const url = `${apiUrl}/funil/set_temperature/${negociation.negociation_id}/${temperature}`;
      // console.log(url);
      api.get(url, true)
        .then(function(response){
          msgs.addSuccess(response.data.msgs)
        })
    }
    vm.change_step = function(negociation){
      negociation.change_step = !negociation.change_step
    }
    vm.set_step = function(negociation, step, index){
      const old_step = negociation.step
      negociation.step = step.toString();
      negociation.change_step = false

      switch(old_step){
        case '0':
          vm.negociations_1.splice(index, 1)
          break;
        case '1':
          vm.negociations_2.splice(index, 1)
          break;
        case '2':
          vm.negociations_3.splice(index, 1)
          break;
        case '3':
          vm.negociations_4.splice(index, 1)
          break;
      }

      switch(step.toString()){
        case '0':
          vm.negociations_1.splice(0, 0, negociation)
          break;
        case '1':
          vm.negociations_2.splice(0, 0, negociation)
          break;
        case '2':
          vm.negociations_3.splice(0, 0, negociation)
          break;
        case '3':
          vm.negociations_4.splice(0, 0, negociation)
          break;
      }
      const url = `${apiUrl}/funil/set_step/${negociation.negociation_id}/${step}`;
      // console.log(url);
      api.get(url, true)
        .then(function(response){
          msgs.addSuccess(response.data.msgs)
        })
    }
    vm.add_schedule = function(negociation){
      // console.log(negociation)
      if(negociation.new_schedule != undefined && negociation.new_schedule != ''){

        const new_schedule = {
          agent_id: vm.me.user_id,
          customer_id: negociation.customer_id,
          negociation_id: negociation.negociation_id,
          historic: negociation.new_schedule,
          lead_step: negociation.step
        }
        const url = `${apiUrl}/schedules`;
        // console.log(url);
        api.post(url, new_schedule, true)
          .then(function(response){
            negociation.schedule_agent_name = vm.me.name
            negociation.schedule_agent_thumb_url = vm.me.photo
            negociation.schedule_date = 'Agora'
            negociation.schedule_historic = negociation.new_schedule
            delete negociation.new_schedule
            msgs.addSuccess(response.data.msgs)
          })

      }

    }
    //filters_delete
    vm.delete_filter_ordenations    = () => {
      if(vm.search_match.or == '') {delete vm.search_match}
      console.log(vm.search_match.or);
    }

    vm.refresh();

  }
})()
