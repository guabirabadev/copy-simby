(function(){
  app.controller('mainCtrl', [
    '$scope',
    '$location',
    'session',
    '$rootScope',
    'token',
    'SweetAlert',
    'api',
    'msgs',
    'audios',
    mainController
  ])

  function mainController ($scope, $location, session, $rootScope, token, SweetAlert, api, msgs, audios) {

    var vm                 = $scope
    var token_data         = token.get_token()

    vm.me                  = $rootScope.me
    vm.isLogin             = session.isLogin
    vm.isLogged            = session.isLogged
    vm.signout             = session.signout
    vm.disconnected        = false
    vm.time_without_action = 0
    vm.time_to_put_offline = 5 // Em minutos
    // vm.config       = {}
    vm.can_to_be_online = {}

    vm.get_me = () => {
      if(!vm.isLogin() && vm.isLogged()){
        api.get(`${apiUrl}/me`, true)
          .then(function(response){
            $rootScope.me = response.data
            vm.me         = $rootScope.me

            vm.me.online_on_chat = parseInt(vm.me.online_on_chat)
            vm.me.online_on_msg = parseInt(vm.me.online_on_msg)
            vm.me.online_on_tel = parseInt(vm.me.online_on_tel)
            // console.log(vm.me)
            // vm.$apply()
          })
      }
    }

    vm.get_notifications = () => {
      if(!vm.isLogin() && vm.isLogged()){
        api.get(`${apiUrl}/notifications?limit=5`, true)
          .then(function(response){
            vm.notifications = response.data
          })
      }
    }

    vm.start = () => {
      if(!vm.isLogin() && vm.isLogged()){
        // vm.get_notifications()
        vm.get_me()
        api.get(`${apiUrl}/cron`, true)
          .then(function(response){
            vm.cron(response.data)
          })
      }

    }

    vm.start();

    vm.configChat = () => {
      vm.can_to_be_online.chat = false;
      for (var i = 0; i < vm.configs.users_who_can_to_be_online.chat.length; i++) {
        const user_id = parseInt(vm.configs.users_who_can_to_be_online.chat[i]);
        if(user_id == vm.me.id){
          vm.can_to_be_online.chat = true;
          return true;
        }
      }
    }

    vm.configMsg = () => {
      vm.can_to_be_online.msg = false;
      for (var i = 0; i < vm.configs.users_who_can_to_be_online.msg.length; i++) {
        const user_id = parseInt(vm.configs.users_who_can_to_be_online.msg[i]);
        if(user_id == vm.me.id){
          vm.can_to_be_online.msg = true;
          return true;
        }
      }
    }

    vm.configTel = () => {
      vm.can_to_be_online.tel = false;
      for (var i = 0; i < vm.configs.users_who_can_to_be_online.tel.length; i++) {
        const user_id = parseInt(vm.configs.users_who_can_to_be_online.tel[i]);
        if(user_id == vm.me.id){
          vm.can_to_be_online.tel = true;
          return true;
        }
      }
    }

    vm.putChatStatusOff = () => {
      audios.ring('stop');
      if(vm.me.online_on_chat){
        vm.me.online_on_chat = false
        api.get(`${apiUrl}/config/set_chat_status_off`, true)
      }
    }
    vm.putChatStatusOn = () => {
      vm.me.online_on_chat = true
      api.get(`${apiUrl}/config/set_chat_status_on`, true)
    }
    vm.chageChatStatus = () => (vm.me.online_on_chat) ? vm.putChatStatusOff() : vm.putChatStatusOn()

    vm.putMsgStatusOff  = () => {
      vm.me.online_on_msg = false
      api.get(`${apiUrl}/config/set_msg_status_off`, true)
    }
    vm.putMsgStatusOn  = () => {
      api.get(`${apiUrl}/config/can_set_msg_status_on`)
        .then(function(response){
          vm.me.online_on_msg = true
          api.get(`${apiUrl}/config/set_msg_status_on`, true)
        }, function(){
          swal("Impedimento", "Você possui clientes que ainda nao foram atendidos!", "error");
        })
    }
    vm.chageMsgStatus  = () => (vm.me.online_on_msg) ? vm.putMsgStatusOff() : vm.putMsgStatusOn()

    vm.putTelStatusOff  = () => {
      vm.me.online_on_tel = false
      api.get(`${apiUrl}/config/set_tel_status_off`, true)
    }
    vm.putTelStatusOn  = () => {
      vm.me.online_on_tel = true
      api.get(`${apiUrl}/config/set_tel_status_on`, true)
    }
    vm.chageTelStatus  = () => (vm.me.online_on_tel) ? vm.putTelStatusOff() : vm.putTelStatusOn()

    vm.isActive = function(path){
      return $location.path().indexOf(path) > -1
    }

    vm.loading = function(){
      return $rootScope.loading
    }

    vm.new_chat = function(chat) {
      audios.ring()
      let autoCancel = setTimeout(function () {
        $('.sweet-alert button.cancel').click()
        vm.putChatStatusOff()
      }, 10000);
      swal({
        title: "Chat",
        text: "Tem um novo cliente no chat!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Atender.",
        cancelButtonText: "Passar a vez.",
        closeOnConfirm: true,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          clearInterval(autoCancel);
          window.location.href = "#!/chat/" + chat.id
        } else {
          clearInterval(autoCancel);
          chat.agent_id = 0;

          api.put(`${apiUrl}/chat`, chat, true)
            .then(function(){
              api.get(`${apiUrl}/cron`, true)
            })

          vm.putChatStatusOff()
          swal("Chat cancelado", "O atendimento foi passado para outro atendente.", "error");
        }
      });

    }

    vm.notifications_to_alert = () => {

      for (var i = 0; i < vm.configs.notifications_to_alert.length; i++) {
        if(vm.configs.notifications_to_alert[i].agent_id == vm.me.id){
          const id = vm.configs.notifications_to_alert[i].id
          swal({
            title: "Alerta",
            text: vm.configs.notifications_to_alert[i].message,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ok.",
            cancelButtonText: "Alertar-me novamente depois.",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm) {
            if (isConfirm) {
              api.get(`${apiUrl}/notifications/mark_as_read/${id}`, true)
            }
          });
        }
      }
    }

    vm.callChat = () => {
      for (var i = vm.configs.chats.length - 1; i >= 0; i--) {
        var chat = vm.configs.chats[i];
        if(chat.agent_id == vm.me.id){
          vm.new_chat(chat)
        }
      }
    }

    vm.cron = (configs) => {
      vm.configs = configs
      vm.configChat()
      vm.configMsg()
      vm.configTel()
      vm.notifications_to_alert()
      vm.callChat()
    }

    vm.mark_all_read = () => {
      vm.notifications.total_unread = 0
      api.put(`${apiUrl}/notifications/all_read`, vm.me, true)
        .then(function(response){
          msgs.addSuccess(response.data.msgs)
        })
    }

    // SOCKET IO

    var socket = io.connect( socket_server );

    socket.on('start-chat', function(chat){
      // console.log('new-chat');
      // console.log(chat)
      // console.log(vm.me)
      if(chat.agent_id == vm.me.id){
        // Fazer verificaçoes para caso de neo estar logado no momento, dedo duro, cancelamento etc
        // vm.start();
        // console.log(vm.config.chat)
        // console.log(chat)
        vm.new_chat(chat)
      }
    });

    socket.on('cron', function(configs){
      // console.log(configs)
      vm.cron(configs)
      // vm.get_notifications()
      vm.$apply()
    });

    // FIM SOCKET IO

    // Força a atualizaçao dos menus toda vez q muda de url
    vm.$watch(function(){
      return $location.path();
    }, function(value){
      // console.log(value);
      // vm.start()
      vm.get_me()
      vm.get_notifications()
    })
    // Fim da forçação de barra

    // Temporizador para colocar os atendimentos em offline caso fique sem mexer no sistema
    // setInterval(function(){
    //   if(!vm.isLogin() && vm.isLogged()){
    //     vm.time_without_action++
    //     if(vm.time_without_action >= vm.time_to_put_offline){
    //       if(vm.me.online_on_chat){
    //         vm.putChatStatusOff()
    //       }
    //       if(vm.me.online_on_msg){
    //         vm.putMsgStatusOff()
    //       }
    //       if(vm.me.online_on_tel){
    //         vm.putTelStatusOff()
    //       }
    //       vm.time_without_action = 0
    //     }
    //   }
    // }, 1000 * 60)

    // $('body').click(function(){
    //   vm.time_without_action = 0
    // })
    // Fim do temporizador

  }
})()
