(function(){
  app.controller('negociationCtrl', [
    '$stateParams',
    'api',
    'msgs',
    '$location',
    'token',
    'formatNumbers',
    negociationController
  ])

  function negociationController($stateParams, api, msgs, $location, token, formatNumbers) {
    const negociation_id   = $stateParams.negociation_id
    const url              = `${apiUrl}/negociations/${negociation_id}`

    var vm                 = this
    vm.actualProduct       = {};
    vm.actualProduct.files = [];
    vm.date_format         = 'dd/MM/yyyy';
    vm.dateOptions = {
      dateDisabled: false,
      formatYear: 'yyyy',
      minDate: new Date(),
      startingDay: 1
    };
    vm.altInputFormats = ['d!/M!/yyyy'];
    vm.open1 = function() {
      vm.popup1.opened = true;
    };
    vm.popup1 = {
      'opened' :  false
    }

    vm.refresh = function(){
      api.get(url)
        .then(function success(response){
          vm.negociation       = response.data
          vm.negociation.value = formatNumbers.numberFromDb(vm.negociation.value)
          vm.actualProduct     = vm.negociation.product;
          vm.customer          = vm.negociation.customer;
          vm.files             = vm.actualProduct.files
          // console.log(response)
          vm.mosaic_url = `mosaics/${vm.negociation.customer.mosaic.toLowerCase()}.html`
          if(parseFloat(vm.negociation.value) == 0){
            swal({
              title: "Cliente sem valor de negociação",
              text: `Este cliente já está sem o valor da negociação. Deseja incluir agora?`,
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-primary",
              confirmButtonText: "Sim.",
              cancelButtonText: "Agora não.",
              closeOnConfirm: true,
              closeOnCancel: true
            },
            function(isConfirm) {
              if (isConfirm) {
                $('#myModalValue').modal('show')
              }
            });
          }
        })
      const url_reasons = `${apiUrl}/reasons`
      api.get(url_reasons)
        .then(function success(response){
          vm.reasons = response.data
        })
      const url_schedules_activities = `${apiUrl}/schedules_activities`
      api.get(url_schedules_activities)
        .then(function success(response){
          vm.schedules_activities = response.data
        })
      const url_schedules_products = `${apiUrl}/products`
      api.get(url_schedules_products)
        .then(function success(response){
          vm.products = response.data
        })
      api.get(`${apiUrl}/me`, true)
        .then(function(response){
          vm.me = response.data
        })

    }

    vm.setTelStatus = function(tel){
      const url = `${apiUrl}/funil/set_status/${tel.id}/${tel.status}`;
      api.get(url, true)
        .then(function(response){
          msgs.addSuccess(response.data.msgs)
        })
    }

    vm.win = function(){
      var formData = vm.win_form
      const url = `${apiUrl}/funil/win/${negociation_id}`;
      api.post(url, formData)
        .then(function(response){
          delete vm.win_form
          msgs.addSuccess(response.data.msgs)
          $('#myModalWin').modal('hide')
          $location.path('funil')
        })
    }

    vm.lose = function(){
      var formData = vm.lose_form
      const url = `${apiUrl}/funil/lose/${negociation_id}`;
      api.post(url, formData)
        .then(function(response){
          delete vm.lose_form
          msgs.addSuccess(response.data.msgs)
          $('#myModalLose').modal('hide')
          $location.path('funil')
        })
    }

    vm.close_contact = function(){
      console.log('vm.close_contact')
      var formData = {
        reason: 0,
        observation: ''
      }
      const url = `${apiUrl}/funil/close_contact/${negociation_id}`;
      api.post(url, formData)
        .then(function(response){
          delete vm.contact_form
          msgs.addSuccess(response.data.msgs)
          $('#myModalContact').modal('hide')
          $location.path('funil')
        })
    }

    vm.give_back = function(){
      var formData = vm.give_back_form
      const url = `${apiUrl}/funil/give_back/${negociation_id}`;
      api.post(url, formData)
        .then(function(response){
          delete vm.give_back_form
          msgs.addSuccess(response.data.msgs)
          $('#myModalGiveBack').modal('hide')
          $location.path('funil')
        })
    }

    function get_schedules_activity(id){
      for (var i = vm.schedules_activities.length - 1; i >= 0; i--) {
        if(vm.schedules_activities[i].id == id){
          return vm.schedules_activities[i].description
        }
      }
      return 'teste'
    }

    vm.add_schedule = function(){
      // console.log(vm.formDataAddSchedule)
      const formDataAddSchedule_date = new Date(vm.formDataAddSchedule.date)
      const formDataAddSchedule_day = `${(formDataAddSchedule_date.getDate() < 10 ? '0' : '')}${formDataAddSchedule_date.getDate()}`
      const formDataAddSchedule_month = `${(formDataAddSchedule_date.getMonth() + 1 < 10 ? '0' : '')}${formDataAddSchedule_date.getMonth() + 1}`
      const new_date = `${formDataAddSchedule_day}/${formDataAddSchedule_month}/${formDataAddSchedule_date.getFullYear()}`
      // console.log(vm.formDataAddSchedule.date)

      const new_schedule = {
        title: vm.customer.name,
        date: new_date,
        time: vm.formDataAddSchedule.time,
        activity_id: vm.formDataAddSchedule.activity,
        historic: vm.formDataAddSchedule.historic,
        agent_id: vm.me.id,
        customer_id: vm.negociation.customer.id,
        negociation_id: vm.negociation.id,
        lead_step: vm.negociation.step,
        show_in_calendar: vm.formDataAddSchedule.show_in_calendar
      }

      // console.log(new_schedule)

      const url = `${apiUrl}/schedules`;
      api.post(url, new_schedule)
        .then(function(response){
          const add_to_negociation = {
            agent_name: vm.me.name,
            date: new_date,
            time: vm.formDataAddSchedule.time,
            historic: vm.formDataAddSchedule.historic,
            activity: get_schedules_activity(vm.formDataAddSchedule.activity),
          }

          vm.negociation.schedules.push(add_to_negociation)

          delete vm.formDataAddSchedule
          msgs.addSuccess(response.data.msgs)
        })
    }

    vm.cancel_add_schedule = function(){
      delete vm.formDataAddSchedule
    }

    vm.setProduct = function(product){
      // console.log(vm.actualProduct)
      // console.log(product)
      // console.log(vm.negociation)
      vm.actualProduct = product
      vm.files         = vm.actualProduct.files
      // vm.maps_url = "https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d7643.150448325902!2d"+ vm.actualProduct.latitude + "!3d" + vm.actualProduct.longitude + "!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1481217841509"
      vm.maps_url = ""
      // console.log(vm.maps_url)
      const url = `${apiUrl}/funil/set_product/${vm.negociation.id}/${product.id}`;
      api.get(url, true)
        .then(function(response){
          msgs.addSuccess(response.data.msgs)
        })
    }

    var indexedCategories = [];

    vm.categoriesToFilter = function() {
        indexedCategories = [];
        return vm.files;
    }

    vm.filterCategories = function(file) {
        var categoryIsNew = indexedCategories.indexOf(file.category) == -1;
        if (categoryIsNew) {
            indexedCategories.push(file.category);
        }
        return categoryIsNew;
    }



    vm.change_value = () => {
      const url = `${apiUrl}/negociations`;
      // console.log(vm.negociation.value)
      const value = formatNumbers.numberToDb(vm.negociation.value)
      // console.log(value)
      const data = {
        'id' : vm.negociation.id,
        'value' : value
      }
      api.put(url, data, true)
        .then(function(response){
          msgs.addSuccess(response.data.msgs)
        })
    }

    vm.refresh();
  }


})()
