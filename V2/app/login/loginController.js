(function(){
  app.controller('loginCtrl', [
    '$location',
    'session',
    loginController
  ])

  function loginController($location, session){
    var vm = this

    vm.login = function(formData){
      session.login(formData)
    }
  }
})()
