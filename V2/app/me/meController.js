(function(){
  app.controller('meCtrl', [
    'api',
    'msgs',
    'FileUploader',
    '$location',
    meController
  ])

  function meController (api, msgs, FileUploader, $location) {
    var vm = this
    const url = `${apiUrl}/me`

    vm.uploader = new FileUploader({
      url: `${apiUrl}/me/upload`,
      headers : {
            'token': sessionStorage.getItem('token')
          }
    });
    // FILTERS
    vm.uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS
    vm.uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        // console.info('onWhenAddingFileFailed', item, filter, options);
    };
    vm.uploader.onAfterAddingFile = function(fileItem) {
        // console.info('onAfterAddingFile', fileItem);
        // fileItem.formData.push({id: vm.me.id});
        fileItem.upload();
    };
    vm.uploader.onAfterAddingAll = function(addedFileItems) {
        // console.info('onAfterAddingAll', addedFileItems);
    };
    vm.uploader.onBeforeUploadItem = function(item) {
        // console.info('onBeforeUploadItem', item);
    };
    vm.uploader.onProgressItem = function(fileItem, progress) {
        // console.info('onProgressItem', fileItem, progress);
    };
    vm.uploader.onProgressAll = function(progress) {
        // console.info('onProgressAll', progress);
        vm.uploader.progress  = progress;
        vm.uploader.uploading = true;
    };
    vm.uploader.onSuccessItem = function(fileItem, response, status, headers) {
        // console.info('onSuccessItem', fileItem, response, status, headers);
    };
    vm.uploader.onErrorItem = function(fileItem, response, status, headers) {
        // console.info('onErrorItem', fileItem, response, status, headers);
    };
    vm.uploader.onCancelItem = function(fileItem, response, status, headers) {
        // console.info('onCancelItem', fileItem, response, status, headers);
    };
    vm.uploader.onCompleteItem = function(fileItem, response, status, headers) {
      vm.refresh(true);
      fileItem = "";
    };
    vm.uploader.onCompleteAll = function() {

        // console.info('onCompleteAll');
        vm.uploader.uploading = false;
        vm.uploader.progress = 0;
    };

    vm.refresh = function(quitely){
      if(quitely == undefined){ quitely = false }
      api.get(url, quitely)
        .then(function success(response){
          // console.log(response.data)
          vm.me = response.data
        })
    }

    vm.save = function(){
      const url = `${apiUrl}/me`;
      api.put(url, vm.me)
        .then(function(response){
          vm.me = response.data.result
          msgs.addSuccess(response.data.msgs)
        })
    }

    vm.choose_photo = function(){
      $('#file_upload').click();
    }

    vm.is_change_pass = function()
    {
      return ($location.path().search('change_pass') != -1)
    }

    vm.change_pass = function(){
      if(vm.me.new_pass !== vm.me.rpt_new_pass){
        msgs.addError('As novas senhas não são iguais.')
        return false;
      }
      vm.save()
    }

    vm.refresh()
  }
})()
