(function(){
  app.controller('dashboardCtrl', [
    'api',
    DashboardController
  ])
  function DashboardController (api) {

    const url      = `${apiUrl}/dashboard/vendas`
    var vm         = this
    vm.pageSize    = 5
    vm.currentPage = 1

    vm.get_total_schedules = () => {
      vm.total_ranking = 0
      for (var i = 0; i < vm.data.ranking.length; i++) {
        vm.total_ranking += parseInt(vm.data.ranking[i].total)
        vm.data.ranking[i].position = i + 1
      }
    }

    vm.get_my_position = () => {
      for(var index in vm.data.ranking){
        if(vm.data.ranking[index].id == vm.me.id){
          vm.my_position = parseInt(index + 1)
        }
      }
    }

    vm.get_me = () => {
      api.get(`${apiUrl}/me`)
        .then(function(response){
          vm.me = response.data
          vm.get_my_position()
        })
    }

    vm.start = function(){
      api.get(url)
        .then(function(response){
          vm.data = response.data
          vm.get_me()
          vm.get_total_schedules()
        })
    }

    vm.start()
  }
})()
