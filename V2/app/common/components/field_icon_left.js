(function(){
  app.component('fieldIconLeft', {
    bindings: {
      id: '@',
      label: '@',
      grid: '@',
      placeholder: '@',
      type: '@',
      model: '=',
      readonly: '<',
      icon: '@',
      required: '@',
    },
    controller : [
    'gridSystem',
    function(gridSystem){
      this.$onInit = () => this.gridClasses = gridSystem.toCssClasses(this.grid)
    }],
    template: `
      <div class="{{ $ctrl.gridClasses }}">
        <div class="form-group">
          <label class="control-label" for="{{ $ctrl.id }}">{{ $ctrl.label }}</label>
          <div class="input-group">
            <span class="input-group-addon"><i class="{{$ctrl.icon}}"></i></span>
            <input
              type="{{ $ctrl.type }}"
              ng-model="$ctrl.model"
              id="{{ $ctrl.id }}"
              class="form-control"
              placeholder="{{ $ctrl.placeholder }}"
              ng-readonly="$ctrl.readonly"
              ng-required = "$ctrl.required"
            >
          </div>
        </div>
      </div>
    `
  })
})()
