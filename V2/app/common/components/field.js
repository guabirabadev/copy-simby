(function(){
  app.component('field', {
    bindings: {
      id: '@',
      label: '@',
      grid: '@',
      placeholder: '@',
      type: '@',
      model: '=',
      readonly: '<',
      required: '@',
    },
    controller : [
    'gridSystem',
    function(gridSystem){
      this.$onInit = () => this.gridClasses = gridSystem.toCssClasses(this.grid)
    }],
    template: `
      <div class="{{ $ctrl.gridClasses }}">
        <div class="form-group">
          <label for="{{ $ctrl.id }}">{{ $ctrl.label }}</label>
          <input
            type="{{ $ctrl.type }}"
            ng-model="$ctrl.model"
            id="{{ $ctrl.id }}"
            class="form-control"
            placeholder="{{ $ctrl.placeholder }}"
            ng-readonly="$ctrl.readonly"
            ng-required = "$ctrl.required"
          >
        </div>
      </div>
    `
  })
})()
