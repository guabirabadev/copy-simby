(function(){
  app.component('infoBox', {
    bindings: {
      grid: '@',
      colorClass: '@',
      value: '@',
      text: '@',
      iconClass: '@',
      progress: '@',
      progressText : '@',

    },
    controller: [
      'gridSystem',
      function(gridSystem) {
        this.$onInit = () => this.gridClasses = gridSystem.toCssClasses(this.grid)
      }
    ],
    template: `
      <div class="{{ $ctrl.gridClasses }}">
        <div class="info-box {{ $ctrl.colorClass }}">
          <span class="info-box-icon"><i class="{{ $ctrl.iconClass }}"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">{{ $ctrl.text }}</span>
            <span class="info-box-number">{{ $ctrl.value }}</span>
            <div class="progress" ng-if="$ctrl.progress">
              <div class="progress-bar" style="width: {{ $ctrl.progress }}%"></div>
            </div>
            <span class="progress-description" ng-if="$ctrl.progressText">
              {{ $ctrl.progressText }}
            </span>
          </div>
        </div>
      </div>
    `
  })
})()
