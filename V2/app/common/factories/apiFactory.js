(function(){
  app.factory('api', [
    '$http',
    'msgs',
    '$rootScope',
    '$location',
    ApiFactory
    ])

  function ApiFactory ($http, msgs, $rootScope, $location) {
    $http.defaults.headers.common['token'] = sessionStorage.getItem('token');

    function set_loading(){
      $rootScope.callSetLoading++;
      $rootScope.loading = true;
    }

    function unset_loading(){
      $rootScope.callSetLoading--;
      if($rootScope.callSetLoading <= 0){
        $rootScope.loading = false;
      }
    }

    function signout(){
      sessionStorage.removeItem('token')
      $location.path('login')
    }

    function isLogin(){
      return $location.path().indexOf('login') > -1
    }

    function isLogged(){
      // console.log(sessionStorage.getItem('token'))
      if(isLogin()){
        return true
      }

      const token = sessionStorage.getItem('token')
      // console.log(token)
      // console.log(token.split('.').length)
      if(
        token == undefined ||
        token == null ||
        token == 'undefined' ||
        token == 'null' ||
        token == '' ||
        token.split('.').length < 3
        )
      {
        signout()
        return false
      }else{
        return true
      }
    }

    function set_errors(response){
      if(!response.data){ msgs.addError(['Erro inesperado.']); return false }
      if(!response.data.errors){ msgs.addError(['Erro inesperado.']); return false }
      msgs.addError(response.data.errors)

      switch(response.status){
        case 401:
          signout();
          break;
        case 404:
          $location.path('dashboard');
          break;
      }
    }

    function get(url, quitely){
      // if(!isLogin() && isLogged()){
        $http.defaults.headers.common['token'] = sessionStorage.getItem('token');
        if(quitely == undefined){ quitely = false }
        if(!quitely){ set_loading() }

        // var config = {
        //     headers : {
        //       'token': sessionStorage.getItem('token')
        //     }
        // }
        var result = $http.get(url);

        result.then(function success(response){
            if(!quitely){ unset_loading() }
          }, function error(response){
            if(!quitely){ unset_loading() }
            if(!isLogin() && isLogged()){ set_errors(response) }
          })

        return result;
      // }
    }

    function post(url, formData, quitely){
      $http.defaults.headers.common['token'] = sessionStorage.getItem('token');
      if(quitely == undefined){ quitely = false }
      if(!quitely){ set_loading() }
      var data = $.param(JSON.parse(angular.toJson(formData)));
      var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
      }

      var result = $http.post(url, data, config);

      result.then(function(response){
        if(!quitely){ unset_loading() }
        // console.log(response)
      }, function(response){
        if(!quitely){ unset_loading() }
        set_errors(response)
      })

      return result;
    }

    function put(url, formData, quitely){
      // isLogged()
      $http.defaults.headers.common['token'] = sessionStorage.getItem('token');
      if(quitely == undefined){ quitely = false }
      if(!quitely){ set_loading() }
      // var data = $.param(JSON.parse(angular.toJson(formData)));
      var data = formData;
      // console.log(data);
      var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
      }

      var result = $http.put(url, data, config);

      result.then(function(response){
        if(!quitely){ unset_loading() }
        // console.log(response)
      }, function(response){
        if(!quitely){ unset_loading() }
        set_errors(response)
      })

      return result;
    }

    function remove(url, quitely){
      // isLogged()
      $http.defaults.headers.common['token'] = sessionStorage.getItem('token');
      if(quitely == undefined){ quitely = false }
      if(!quitely){ set_loading() }
      // var data = $.param(JSON.parse(angular.toJson(formData)));
      // var data = formData;
      // console.log(data);
      var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
      }

      var result = $http.delete(url, config);

      result.then(function(response){
        if(!quitely){ unset_loading() }
        // console.log(response)
      }, function(response){
        if(!quitely){ unset_loading() }
        set_errors(response)
      })

      return result;
    }

    return{
      get,
      post,
      put,
      remove,
      signout,
      isLogged,
      isLogin
    }

  }
})()
