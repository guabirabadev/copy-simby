(function(){
  app.factory('session', [
    '$location',
    'api',
    'msgs',
    '$rootScope',
    sessionFactory
  ])

  function sessionFactory ($location, api, msgs, $rootScope) {

    function isLogin(){
      return api.isLogin()
    }

    function login(formData){
      const url = `${apiUrl}/session`
      api.post(url, formData)
        .then(function(response){
          if(response.data.token){
            sessionStorage.setItem('token', response.data.token)
            const url = `${apiUrl}/me`;
            api.get(url)
              .then(function(response){
                $rootScope.me = response.data
              })
            $location.path('dashboard')
          }else{
            msgs.addError(['Erro inesperado.'])
          }
        })
    }

    function verify(){
      if(window.location.search != ''){
        const token = window.location.search.split('?v=')
        if(token.length > 1){
          sessionStorage.setItem('token', token[1])
          // console.log(document.location)
          const url_redirect = `${document.location.origin}${document.location.pathname}`
          document.location.href = url_redirect;
          // $location.path('dashboard')
        }
      }
    }

    // Fui obrigado e enviar para a api pq precisava dela para tratar os erros
    function signout(){
      return api.signout()
    }

    function isLogged(){
      return api.isLogged()
    }

    return{
      isLogin,
      isLogged,
      signout,
      login,
      verify
    }

  }
})()
