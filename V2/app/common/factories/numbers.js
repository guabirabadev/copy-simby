(function(){
  app.factory('formatNumbers', [numbersFactory])

  function numbersFactory(){

    function numberFromDb (number) {
      return parseFloat(number).formatFloat(2, ',', '.')
    }

    function numberToDb (number) {
      // console.log(number)
      while(number.indexOf('.') > 0){
        number = number.replace('.', '')
        // console.log(number)
      }
      number = number.replace(',', '')
      // console.log(number)
      number = parseFloat(number)/100
      // console.log(number)
      return number.formatFloat(2, '.', '')
      // return (parseFloat(number.replace('.', '').replace(',', ''))/100).formatFloat(2, '.', '')
    }

    return {
      numberFromDb,
      numberToDb
    };
  }
})()
