(function(){
  app.factory('audios', [audiosFatory])

  function audiosFatory(){

    var audio_ring = new Audio('assets/audios/bell.mp3');
    audio_ring.addEventListener('ended', function() {
      this.currentTime = 0;
      this.play();
    }, false);

    function ring(status = 'start'){
      // console.log(status)
      if(status == 'start'){
        audio_ring.play();
      }

      if(status == 'stop'){
        audio_ring.pause();
        audio_ring.currentTime = 0;
      }
    }

    return {
      ring
    };

  }
})()
