(function(){
  app.controller('importCtrl', [
    '$scope',
    'api',
    importController
  ])

  function importController($scope, api){

    var vm         = $scope
    vm.actual_step = 1;
    vm.rows        = [];
    vm.rows_       = [];
    vm.titles_     = [];
    vm.obj_keys    = [];
    vm.cols        = [
      {value: 'name', title: 'Nome'},
      {value: 'tels', title: 'Telefones'},
      {value: 'cpf', title: 'CPF'},
    ];
      // console.log("actual_step")
      // console.log(vm.actual_step)
    // vm.file_upload_

    vm.file_upload_click = () => $('#file_upload').click();
    vm.refresh = () => window.location.reload()
    vm.organizing_rows = (obj) => {
      vm.rows = obj
      var obj_keys = []
      for (var i = 0; i < obj.length; i++) {
        var keys = Object.keys(obj[i])
        for (var i_ = 0; i_ < keys.length; i_++) {
          if(obj_keys.indexOf(keys[i_]) == -1){
            obj_keys.push(keys[i_])
          }
        }
      }

      for (var i = 0; i < obj_keys.length; i++) {
        vm.obj_keys.push({'table' : obj_keys[i], 'system': 0})
      }
      // console.log(vm.obj_keys)
    }

    // $scope.$watch(() => vm.file_upload_, function (newVal) {
    //   console.log(newVal);
    // });

     $('#file_upload').on('change', function (oEvent) {
      // console.log("actual_step")
      // console.log(vm.actual_step)
      // var files = $(event.currentTarget).get(0).files;

      // Get The File From The Input
      // var oFile = oEvent.target.files[0];
      // var sFilename = oFile.name;
      // // Create A File Reader HTML5
      // var reader = new FileReader();

      // // Ready The Event For When A File Gets Selected
      // reader.onload = function(e) {
      //   var data = e.target.result;
      //   var cfb = XLS.CFB.read(data, {type: 'binary'});
      //   console.log(cfb)
      //   var wb = XLS.parse_xlscfb(cfb);
      //   // Loop Over Each Sheet
      //   wb.SheetNames.forEach(function(sheetName) {
      //       // Obtain The Current Row As CSV
      //       var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
      //       var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);

      //       // $("#file_upload").html(sCSV);
      //       console.log(oJS)
      //   });
      // };

      // Tell JS To Start Reading The File.. You could delay this if desired
      // reader.readAsBinaryString(oFile);

      var file = oEvent.target.files[0];
      var reader = new FileReader();

      reader.onload = function(e){
          var data = e.target.result;
          var workbook = XLSX.read(data, {type : 'binary'});

          var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[workbook.SheetNames[0]]);
            vm.actual_step = 2;
            vm.organizing_rows(XL_row_object);
            vm.$apply()
          // workbook.SheetNames.forEach(function(sheetName){
          //     // Here is your object
          //   vm.actual_step = 2;
          //   console.log("actual_step")
          //   console.log(vm.actual_step)
          //     var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          //     console.log(XL_row_object);
          //     // var json_object = JSON.stringify(XL_row_object);
          //     // console.log(json_object);

          // })
      };

      reader.onerror = function(ex){
          console.log(ex);
      };

      reader.readAsBinaryString(file);

    });

     vm.reorder = () => {
      vm.titles_ = [];
      for (var i_obj_keys = 0; i_obj_keys < vm.obj_keys.length; i_obj_keys++) {
        if(vm.obj_keys[i_obj_keys].system != 0){
          for (var i_cols = 0; i_cols < vm.cols.length; i_cols++) {
            if(vm.cols[i_cols].value == vm.obj_keys[i_obj_keys].system){

              var can_add = true;
              for (var i_titles_ = 0; i_titles_ < vm.titles_.length; i_titles_++) {
                if(vm.titles_[i_titles_].value == vm.cols[i_cols].value){
                  can_add = false
                }
              }

              if(can_add){
                var title = {
                  'value' : vm.cols[i_cols].value,
                  'title' : vm.cols[i_cols].title,
                }
                vm.titles_.push(title)
              }
            }
          }
        }
      }

      vm.rows_ = [];
      for (var i_row = 0; i_row < vm.rows.length; i_row++) {
        vm.rows[i_row]
        var row = {}
        for (var i_obj_keys = 0; i_obj_keys < vm.obj_keys.length; i_obj_keys++) {
          if(vm.obj_keys[i_obj_keys].system != 0){
            row[vm.obj_keys[i_obj_keys].system] = vm.rows[i_row][vm.obj_keys[i_obj_keys].table]
            row.statuses = ['Aguardando.']
            row.origin_id = 33
          }
        }
        vm.rows_.push(row);
      }
     }

    var cont_insert = 0;
    vm.insert_next = () => {
      if(cont_insert < vm.rows_.length - 1){
        cont_insert++;
        vm.insert()
      }else{
        vm.actual_step = 4;
      }
      vm.$apply()
    }

    vm.insert = () => {
      // console.log(cont_insert)
      vm.rows_[cont_insert].statuses = ['Inserindo']
      const url = `${apiUrl}/customers`;
      // console.log(url)
      // console.log(vm.rows_[cont_insert])
      api.post(url, vm.rows_[cont_insert])
        .then(function(response){
          vm.rows_[cont_insert].class = 'success'
          vm.rows_[cont_insert].statuses = response.data.msgs
          vm.insert_next();
          // console.log(response)
        }, function(response){
          vm.rows_[cont_insert].class = 'danger'
          vm.rows_[cont_insert].statuses = response.data.errors
          vm.insert_next();
          // console.log(response)
        })

      // setTimeout(function(){
      //   vm.rows_[index].class = 'success'
      //   vm.rows_[index].statuses = response.data.msgs
      //   if(index < vm.rows_.length - 1){
      //     cont++;
      //     vm.insert(cont)
      //   }else{
      //     vm.actual_step = 4;
      //   }
      //   vm.$apply()
      // }, 1000)
    }

    vm.import = () => {
      // console.log('import')
      vm.actual_step = 3;
      vm.insert(cont_insert)
      // var cont = 0;
      // var teste = setInterval(function(){
      //   cont++;
      //   if(cont >= 3){
      //     clearInterval(teste)
      //   }
      // }, 1000)
    }

  }
})()
