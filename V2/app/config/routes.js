(function(){
  app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider){
      $stateProvider
      .state('login',{
        url: "/login",
        templateUrl: "login/login.html"
      })
      .state('recovery',{
        url: "/recovery",
        templateUrl: "login/recovery.html"
      })
      .state('dashboard',{
        url: "/dashboard",
        templateUrl: "dashboard/dashboard.html"
      })
      .state('me',{
        url: "/me",
        templateUrl: "me/main.html"
      })
      .state('change_pass',{
        url: "/me/change_pass",
        templateUrl: "me/main.html"
      })
      .state('funil',{
        url: "/funil",
        templateUrl: "funil/list.html"
      })
      .state('funil/:negociation_id',{
        url: "/funil/:negociation_id",
        templateUrl: "negociation/details.html"
      })
      .state('chat/:id',{
        url: "/chat/:id",
        templateUrl: "chat/main.html"
      })
      .state('calendar',{
        url: "/calendar",
        templateUrl: "calendar/main.html"
      })
      .state('calendar/add',{
        url: "/calendar/add",
        templateUrl: "calendar/form.html"
      })
      .state('calendar/:id',{
        url: "/calendar/:id",
        templateUrl: "calendar/form.html"
      })
      .state('contacts',{
        url: "/contacts",
        templateUrl: "customers/main.html"
      })
      .state('contacts/add',{
        url: "/contacts/add",
        templateUrl: "customers/form.html"
      })
      .state('contacts/:id',{
        url: "/contacts/:id",
        templateUrl: "customers/details.html"
      })
      .state('import',{
        url: "/import",
        templateUrl: "import/main.html"
      })
      .state('enrichiement',{
        url: "/enrich",
        templateUrl: "enrich/dashboard.html"
      })
      .state('enrichiement/:id',{
        url: "/enrichiement/:id",
        templateUrl: "enrich/dashboard.html"
      })
      .state('enrichiement/:id/serasa',{
        url: "/enrichiement/:id/serasa",
        templateUrl: "enrich/enriching-serasa.html"
      })
      .state('enrichiement/:id/transparencia',{
        url: "/enrichiement/:id/transparencia",
        templateUrl: "enrich/enriching-ptransparencia.html"
      })
      .state('enrichiement/:id/empresas',{
        url: "/enrichiement/:id/empresas",
        templateUrl: "enrich/enriching-empresas.html"
      })
      .state('enrichiement/:id/facebook',{
        url: "/enrichiement/:id/facebook",
        templateUrl: "enrich/enriching-facebook.html"
      })
      .state('enrichiement/:id/twitter',{
        url: "/enrichiement/:id/twitter",
        templateUrl: "enrich/enriching-twitter.html"
      })
      .state('enrichiement/:id/gplus',{
        url: "/enrichiement/:id/gplus",
        templateUrl: "enrich/enriching-gplus.html"
      })
      .state('enrichiement/:id/escolaridade',{
        url: "/enrichiement/:id/escolaridade",
        templateUrl: "enrich/enriching-escolaridade.html"
      })


      $urlRouterProvider.otherwise('/dashboard')
    }
  ])
})()
