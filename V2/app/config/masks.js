 function set_mask() {
  setTimeout(function(){
    jQuery('[cpf-mask]').unmask().mask('000.000.000-00');
    jQuery('[cep-mask]').unmask().mask('00.000-000');
    jQuery('[phone-mask]').unmask().mask('(00) 0000-0000');
    jQuery('[default-phone-mask]').unmask().mask('00000-0000', {reverse: true});
    jQuery('[cel-mask]').unmask().mask('(00) 00000-0000');
    jQuery('[date-mask]').unmask().mask('00/00/0000');
    jQuery('[time-mask]').unmask().mask('00:00');
    jQuery('[money-mask]').unmask().mask('#.##0,00', {reverse: true});
  },50);
}
set_mask();

Number.prototype.formatFloat = function(c, d, t){
  var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
// numeral.language('br', {
//     delimiters: {
//         thousands: '.',
//         decimal: ','
//     },
//     abbreviations: {
//         thousand: 'k',
//         million: 'm',
//         billion: 'b',
//         trillion: 't'
//     },
//     ordinal : function (number) {
//         return number === 1 ? 'er' : 'ème';
//     },
//     currency: {
//         symbol: 'R$'
//     }
// });

// // switch between languages
// numeral.language('br');

