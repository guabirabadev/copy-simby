app.filter('estimatedIncome', function() {

  // In the return function, we must pass in a single parameter which will be the data we will work on.
  // We have the ability to support multiple other parameters that can be passed into the filter optionally
  return function(input) {

    const faixa = 2000;
    const faixas = Math.round(parseFloat(input) / faixa);
    const ate = faixas * faixa;
    const de = ate - faixa;

    return `entre R$${de.formatFloat(2, ',', '.')} e R$${ate.formatFloat(2, ',', '.')}`;

  }

});
