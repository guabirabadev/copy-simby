(function(){
  app.controller('customerCtrl', [
    'api',
    '$location',
    '$stateParams',
    '$scope',
    'SweetAlert',
    'msgs',
    '$http',

    customerController
  ])

  function customerController(api, $location, $stateParams, $scope, SweetAlert, msgs, $http){
    const { id     = 0 } = $stateParams
    var vm         = this
    vm.currentPage = 1
    vm.pageSize    = 12

    vm.fetch_all = function(){
      const url = `${apiUrl}/customers`;
      api.get(url)
        .then(function(response){
          vm.customers = response.data
        })

    }

    vm.fetch = function(){
      const url = `${apiUrl}/customers/${id}`;
      api.get(url)
        .then(function(response){
          vm.customer = response.data
          vm.mosaic_url = `mosaics/${vm.customer.mosaic.toLowerCase()}.html`
          //console.log(vm.customer)
        })
    }

    vm.get_me = () => {
      api.get(`${apiUrl}/me`)
        .then(function(response){
          vm.me = response.data
        })
        //console.log(vm.me);
    }

    vm.refresh = function(){
      vm.get_me()
      if($location.path().search('add') == -1){
        if(id === 0){
          vm.fetch_all();
        }else{
          vm.fetch();
        }
      }else{
        delete vm.customer
        vm.customer = {tels: [{}], observations: [{}]};
      }
    }

    vm.addTel = function(index){
      vm.customer.tels.splice(index + 1, 0, {})
    }

    vm.cloneTel = function(index, {name, value}){
      vm.customer.tels.splice(index + 1, 0, {name, value})
    }

    vm.deleteTel = function(index){
      vm.customer.tels.splice(index, 1)
      if(vm.customer.tels.length < 1){
        vm.customer.tels.splice(index, 0, {})
      }
    }

    vm.addObs = function(index){
      vm.customer.observations.splice(index + 1, 0, {})
    }

    vm.cloneObs = function(index, {name, value}){
      vm.customer.observations.splice(index + 1, 0, {name, value})
    }

    vm.deleteObs = function(index){
      vm.customer.observations.splice(index, 1)
      if(vm.customer.observations.length < 1){
        vm.customer.observations.splice(index, 0, {})
      }
    }

    vm.add = (redirect = true) => {
      const url = `${apiUrl}/customers`;
      // console.log(url)
      vm.customer.origin_id = 33
      api.post(url, vm.customer)
        .then(function(response){
          vm.refresh()
          const new_negociation = response.data.negociation_id;
         if (redirect) {
           $location.path(`funil/${new_negociation}`);
         }
        })
    }

    vm.report = (response) => {
      const agents = response.data.agents
      const customer = response.data.customer
      for (var i = 0; i < agents.length; i++) {
        if(agents[i].last_contact_days != '' && parseInt(agents[i].last_contact_days) <= 30){
          const setup = {
            'to_agent' : agents[i].id,
            'message' : `${customer.name} foi cadastrado por ${vm.me.name}`,
            'alert' : '1',
          }
          const url = `${apiUrl}/notifications`;
          api.post(url, setup, true)
        }
      }
    }

    vm.create = function(){
      vm.add(false)
      const url = `${apiUrl}/customers/exists`;
      api.post(url, vm.customer)
        .then(function(response){

          const agents = (response.data.agents != undefined) ? response.data.agents : []

          let agent_names = ''

          let last_contacts = ''

          for (var i = 0; i < agents.length; i++) {

            if(agents[i].last_contact_days != '' && parseInt(agents[i].last_contact_days) <= 30){
              if(agent_names != ''){

                agent_names += ` e `

              }
              agent_names += `${agents[i].name}`

              if(last_contacts != ''){
                last_contacts += ` e `

              }
              last_contacts += `${agents[i].last_contact}`;

            }
          }

           //if(response.data.exists){
            // console.log("agent_names "+agent_names)
            // console.log("last_contacts "+last_contacts)
          if(response.data.exists && agent_names != '' && last_contacts != ''){
            swal({
              title: "Cliente já cadastrado",
              text: `Este cliente já está cadastrado e sendo atendido por ${agent_names}! Último(s) atendimento(s) ${last_contacts}, respectivamente.`,
              type: "warning",
              showCancelButton: false,
              confirmButtonClass: "btn-primary",
              confirmButtonText: "Continuar.",
              cancelButtonText: "Cancelar.",
              closeOnConfirm: true,
              closeOnCancel: false
            },
            function(isConfirm) {
              if (isConfirm) {
                vm.report(response)
                //vm.add()
                $location.path(`funil/${new_negociation}`);
              }
            });
          }
        })
    }

    vm.updateMail = (mail) => {
      var id = $stateParams.id
      console.log(id);
      vm.customer = mail
      console.log(mail);
      const url = `${apiUrl}/customers/update/${id}`;
      $http.put(url, vm.customer)
            .then(function success(data){
              swal("Sucesso!", "E-mail atualizado com sucesso!", "success")
                vm.fetch()
            }, function error(err) {
              alert("Erro ao cadastar email", err)
            })
    }

    vm.new_negociation = (customer) => {
      const url = `${apiUrl}/funil/start_negociation/${customer.id}`;
      api.get(url)
        .then(function(response){
          // console.log(response)
          const new_negociation = response.data.negociation_id;
          $location.path(`funil/${new_negociation}`);
        })
    }

    vm.delete_negociation = (customer) => {
      swal({
        title: "Excluir contato",
        text: `Tem certeza que deseja excluir este contato?.`,
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Continuar.",
        cancelButtonText: "Cancelar.",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
          const url = `${apiUrl}/customers/${customer.id}`;
          api.remove(url)
            .then(function(response){
              msgs.addSuccess(response.data.msgs)
              vm.refresh()
              // console.log(response)
              // const new_negociation = response.data.negociation_id;
              // $location.path(`funil/${new_negociation}`);
            })
        }
      });
    }

    vm.refresh()

    // $scope.$watch(angular.bind(vm, function () {
    //   return vm.customer.mail;
    // }), function (newVal) {
    //   console.log('Email changed to ' + newVal);
    // });
  }
})()
