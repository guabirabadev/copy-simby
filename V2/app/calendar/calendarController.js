(function(){
  app.controller('calendarCtrl', [
    'api',
    '$scope',
    '$location',
    '$stateParams',
    'msgs',
    'SweetAlert',
    calendarController
  ]);

  function calendarController(api, $scope, $location, $stateParams, msgs, SweetAlert){

    const { id = 0 } = $stateParams
    var vm = this

    vm.formaSchedule = (start = null, end = null, schedule = null) => {
      if(start === null){
        start = new Date();
      }

      if(end === null){
        end = new Date();
        end.setHours(end.getHours() + 1)
      }

      var hours = (start.getHours() > 9) ? start.getHours() : `0${start.getHours()}`
      var day = (start.getDate() > 9) ? start.getDate() : `0${start.getDate()}`
      var month = (start.getMonth() + 1 > 9) ? start.getMonth() + 1 : `0${start.getMonth() + 1}`
      var year = start.getFullYear()


      var hours_end = (end.getHours() > 9) ? end.getHours() : `0${end.getHours()}`
      var day_end = (end.getDate() > 9) ? end.getDate() : `0${end.getDate()}`
      var month_end = (end.getMonth() + 1 > 9) ? end.getMonth() + 1 : `0${end.getMonth() + 1}`
      var year_end = end.getFullYear()

      if(schedule === null){
        vm.formDataAddSchedule = {
          'id' : 0,
          'show_in_calendar' : true,
          'alert_week_before' : false,
          'alert_day_before' : false,
          'alert_hour_before' : false,
          'alert_at_time' : false,
          'date' : `${day}/${month}/${year}`,
          'time' : `${hours}:00`,
          'date_end' : `${day_end}/${month_end}/${year_end}`,
          'time_end' : `${hours_end}:00`,
          'color' : ''
        }
      }else{
        vm.formDataAddSchedule = {
          'id' : schedule.id,
          'title' : schedule.title,
          'show_in_calendar' : true,
          'all_day' : (schedule.all_day == '1') ? true : false,
          'alert_week_before' : (schedule.alert_week_before == '1') ? true : false,
          'alert_day_before' : (schedule.alert_day_before == '1') ? true : false,
          'alert_hour_before' : (schedule.alert_hour_before == '1') ? true : false,
          'alert_at_time' : (schedule.alert_at_time == '1') ? true : false,
          'activity' : schedule.activity,
          'historic' : schedule.historic,
          'color' : schedule.color,
          'date' : `${day}/${month}/${year}`,
          'time' : `${hours}:00`,
          'date_end' : `${day_end}/${month_end}/${year_end}`,
          'time_end' : `${hours_end}:00`,
        }
      }
    }


    vm.alertEventOnClick = (date, jsEvent, view) => {
      $location.path(`calendar/${date.id}`)
    }

    vm.alertOnDrop = function(event, delta, revertFunc, jsEvent, ui, view){
      // console.log(vm.formDataAddSchedule)
      // console.log(event)
      // console.log(event.start)
      let d = event.start._d
      d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 );

      let d_end = event.end._d
      d_end.setTime( d_end.getTime() + d_end.getTimezoneOffset()*60*1000 );

      vm.formaSchedule(d, d_end, event)
      // console.log(vm.formDataAddSchedule)

      vm.save()

      // Parei tentando arrumar as horas quando altera o evento

      // vm.calendar.fullCalendar('refetchEvents')
    };

    vm.alertOnResize = function(event, delta, revertFunc, jsEvent, ui, view ){
       // console.log(vm.formDataAddSchedule)
      // console.log(event)
      // console.log(event.start)
      let d = event.start._d
      d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 );

      let d_end = event.end._d
      d_end.setTime( d_end.getTime() + d_end.getTimezoneOffset()*60*1000 );

      vm.formaSchedule(d, d_end, event)
      // console.log(vm.formDataAddSchedule)

      vm.save()
    };

    vm.eventSources = [];
    vm.uiConfig = {
        calendar:{
          height: 450,
          editable: true,
          header:{
            left: 'month agendaWeek agendaDay',
            center: 'title',
            right: 'today prev,next'
          },
          eventClick: vm.alertEventOnClick,
          eventDrop: vm.alertOnDrop,
          eventResize: vm.alertOnResize,
          events: function(start, end, timezone, callback) {
            const date_start = `${ start._d.getFullYear() }-${ start._d.getMonth() + 1 }-${ start._d.getDate() }`
            const date_end = `${ end._d.getFullYear() }-${ end._d.getMonth() + 1 }-${ end._d.getDate() }`
            const url_schedules = `${apiUrl}/schedules?start=${date_start}&end=${date_end}`
            api.get(url_schedules)
              .then(function(response){
                let events = []
                for (var i = 0; i < response.data.length; i++) {
                  events.push(response.data[i])
                  if(events[i].allDay == '1'){
                    events[i].allDay = true
                  }else{
                    events[i].allDay = false
                  }
                }
                callback(events)
              })
          }
        }
      };


    vm.formaSchedule()

    vm.start = () => {
      if(id !== 0){
        const url_schedule = `${apiUrl}/schedules/${id}`
        api.get(url_schedule)
        .then(function success(response){

          let d = new Date(response.data.start)
          d.setTime( d.getTime() + d.getTimezoneOffset()*60*1000 );

          let d_end = new Date(response.data.end)
          d_end.setTime( d_end.getTime() + d_end.getTimezoneOffset()*60*1000 );

          vm.formaSchedule(d, d_end, response.data)
        })
      }

      let url_schedules_activities = `${apiUrl}/schedules_activities`
      api.get(url_schedules_activities)
        .then(function success(response){
          vm.schedules_activities = response.data
        })
    }

    vm.start()

    vm.save = () => {
      const url = `${apiUrl}/schedules`
      if(vm.formDataAddSchedule.id == 0){
        api.post(url, vm.formDataAddSchedule)
          .then(function(response){
            vm.formaSchedule()
            msgs.addSuccess(response.data.msgs)
          })
      }else{
        api.put(url, vm.formDataAddSchedule)
          .then(function(response){
            vm.formaSchedule()
            msgs.addSuccess(response.data.msgs)
          })
      }
      $location.path('calendar')
    }

    vm.delete = () => {
      swal({
        title: "Excluir",
        text: "Tem certeza que deseja excluir esse agendamento?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Excluir",
        cancelButtonText: "Cancelar",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
          const url = `${apiUrl}/schedules/${vm.formDataAddSchedule.id}`
          api.remove(url)
              .then(function(response){
                msgs.addSuccess(response.data.msgs)
                $location.path('calendar')
              })
        }
      });
    }

    vm.cancel = () => {
      $location.path('calendar')
    }

    angular.element('#color-chooser > li > a').click(function(e){
      e.preventDefault();
      const color = jQuery(this).css('color')
      // var appElement = document.querySelector('[ng-app=app]');
      // var $scope = angular.element(appElement).scope();
      $scope.$apply(function() {
        vm.formDataAddSchedule.color = color
        // jQuery('.box.box-default').css('border-top-color', vm.formDataAddSchedule.color)
      });
    })

    $scope.$watch(function(){
      return vm.formDataAddSchedule.color;
    }, function(value){
      if(value != ''){
        jQuery('.box.box-default').css('border-top-color', vm.formDataAddSchedule.color)
      }
    })

    // vm.actualDate = $('#fullCalendar_').fullCalendar( 'get_date' )

  }
})()

