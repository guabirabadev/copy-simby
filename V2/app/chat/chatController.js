(function(){
  app.controller('chatCtrl', [
    '$scope',
    'api',
    '$stateParams',
    '$sce',
    'audios',
    chatController
  ])

  function chatController($scope,api, $stateParams, $sce, audios){
    // console.log(document.URL)
    // var vm         = this
    var vm         = $scope
    vm.trustAsHtml = $sce.trustAsHtml;
    const chat_id  = $stateParams.id

    vm.wellComeMessage = () => vm.sendMessage(`Bem vindo ${vm.chat.customer.name}! Em que posso ajudar?`)
    vm.set_status_atendendo = () => api.get(`${apiUrl}/chat/set_status_atendendo/${vm.chat.id}`, true)
    vm.set_status_concluido = () => {
        api.get(`${apiUrl}/chat/set_status_concluido/${vm.chat.id}`, true)
          .then(function(){
            vm.chat.chat_status_id = '3'
            socket.emit('close-chat-server', vm.chat.id)
          })
      }
    // vm.wellComeMessage = () => console.log(`Bem vindo ${vm.chat.customer.name}! Em que posso ajudar?`)

    vm.start = function(){
      audios.ring('stop');
      const url_chat = `${apiUrl}/chat/${chat_id}`
      api.get(url_chat)
        .then(function success(response){
          vm.chat = response.data
          if(vm.chat.chat_status_id == '1'){
            vm.set_status_atendendo()
            vm.wellComeMessage()
          }
          // console.log(vm.chat)
        })

      const url_messages = `${apiUrl}/chat/${chat_id}/messages`
      api.get(url_messages)
        .then(function success(response){
          vm.messages = response.data
        })

      const url_products = `${apiUrl}/products`
      api.get(url_products)
        .then(function success(response){
          vm.products = response.data
          vm.actualProduct = {}
          vm.actualProduct.files = []
          vm.files = vm.actualProduct.files
        })

      const url_me = `${apiUrl}/me`
      api.get(url_me)
        .then(function success(response){
          vm.me = response.data
        })
    }

    vm.setProduct = function(product){
      // console.log(vm.actualProduct)
      // console.log(product)
      // console.log(vm.negociation)
      vm.actualProduct = product
      vm.files         = vm.actualProduct.files
      // vm.maps_url = "https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d7643.150448325902!2d"+ vm.actualProduct.latitude + "!3d" + vm.actualProduct.longitude + "!3m2!1i1024!2i768!4f13.1!5e0!3m2!1spt-BR!2sbr!4v1481217841509"
      vm.maps_url = ""
      // console.log(vm.maps_url)
    }

    var indexedCategories = [];

    vm.categoriesToFilter = function() {
        indexedCategories = [];
        return vm.files;
    }

    vm.filterCategories = function(file) {
        var categoryIsNew = indexedCategories.indexOf(file.category) == -1;
        if (categoryIsNew) {
            indexedCategories.push(file.category);
        }
        return categoryIsNew;
    }

    vm.empty = function(value){
      if(value === ''){
        return true
      }
      if(value === null){
        return true
      }
      if(value === undefined){
        return true
      }
    }

    vm.sendFileToChat = function(file){
      if(file.is_image == '1'){
        var new_message = `
          <img class="attachment-img" width="300" src="${file.file}">
          <br>
          <br>
          <a href="${file.file}" target="_blank">
            ${file.file}
          </a>
        `
      }else{
        var new_message = `
          <a href="${file.file}" target="_blank">
            ${file.file}
          </a>
        `
      }
      vm.sendMessage(new_message)
    }

    vm.sendMessage = function(new_message){
      if(vm.empty(new_message)){
        new_message = vm.new_message
      }
      const date = new Date()
      const set_new_messate = {
        'content': new_message
      }
      const url = `${apiUrl}/chat/${chat_id}/messages`
      api.post(url, set_new_messate, true)
        .then(function(response){
          socket.emit('chat-new-message-server', response.data)
        })
    }

    vm.start()

    socket.on('chat-new-message', function(message){
      $('#new_message').val('')
      vm.messages.push(message)
      vm.$apply()
    });
  }
})()
