const gulp = require('gulp')
const uglify = require('gulp-uglify')
const uglifycss = require('gulp-uglifycss')
const concat = require('gulp-concat')

gulp.task('deps', ['deps.js', 'deps.css', 'deps.fonts'])

gulp.task('deps.js', function() {
  gulp.src([
    'vendor/modernizr.min.js',
    'node_modules/admin-lte/plugins/jQuery/jquery-2.2.3.min.js',
    'node_modules/angular/angular.min.js',
    'node_modules/angular-ui-router/release/angular-ui-router.min.js',
    'node_modules/angular-animate/angular-animate.min.js',
    'node_modules/angular-toastr/dist/angular-toastr.tpls.min.js',
    'node_modules/admin-lte/bootstrap/js/bootstrap.min.js',
    'node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js',
    'node_modules/admin-lte/dist/js/app.min.js',
    'node_modules/angular-sweetalert/node_modules/sweetalert/lib/sweet-alert.js',
    'node_modules/angular-sweetalert/SweetAlert.js',
    'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
    'node_modules/admin-lte/plugins/fastclick/fastclick.js',
    'node_modules/jquery-mask-plugin/jquery.mask.js',
    'bower_components/moment/min/moment.min.js',
    'bower_components/angular-ui-calendar/src/calendar.js',
    'bower_components/fullcalendar/dist/fullcalendar.min.js',
    'bower_components/fullcalendar/dist/lang/pt-br.js',
    'bower_components/fullcalendar/dist/gcal.js',
    'node_modules/angular-file-upload/dist/angular-file-upload.min.js',
    'node_modules/socket.io-client/dist/socket.io.min.js',
    'node_modules/angular-sanitize/angular-sanitize.js',
  ])
  .pipe(uglify())
  .pipe(concat('deps.min.js'))
  .pipe(gulp.dest('public/assets/js'))
})

gulp.task('deps.css', function() {
  gulp.src([
    'vendor/normalize.min.css',
    'node_modules/angular-toastr/dist/angular-toastr.min.css',
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/admin-lte/bootstrap/css/bootstrap.min.css',
    'node_modules/admin-lte/dist/css/AdminLTE.min.css',
    'node_modules/admin-lte/dist/css/skins/_all-skins.min.css',
    'node_modules/angular-sweetalert/node_modules/sweetalert/lib/sweet-alert.css',
    'node_modules/admin-lte/plugins/fullcalendar/fullcalendar.min.css',
    'bower_components/fullcalendar/dist/fullcalendar.css'
  ])
  .pipe(uglifycss({ "uglyComments": true }))
  .pipe(concat('deps.min.css'))
  .pipe(gulp.dest('public/assets/css'))
})

gulp.task('deps.fonts', function() {
  gulp.src([
    'node_modules/font-awesome/fonts/*.*',
    'node_modules/admin-lte/bootstrap/fonts/*.*'
  ])
  .pipe(gulp.dest('public/assets/fonts'))
})
